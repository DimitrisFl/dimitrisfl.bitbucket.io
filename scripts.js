const resourceUrl = 'https://rickandmortyapi.com/api/character/';
const characters = document.getElementById('characters');
const singleCharacter = document.getElementById('single-character');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
//const modal = document.getElementById('modal');


axios.
get(resourceUrl).
then(({ data: { results } }) => {
  const html = results.map((item) =>
    `<div class='character'>
        <div>
          <img src="${item.image}" alt="" />
        </div>
        <div class = 'character-info' data-characterID = '${item.id}'>
           <span class ="title"> ${item.name}</span> 
       <br/>      

       <span class ="id"> 
         <i class="fa fa-circle" style="background-color : {($item.status === 'Alive'? 'green' : ($item.status === 'Dead' ? 'red' : 'grey'))}; padding-right: 3px;" aria-hidden="true"></i>${item.status}-<span> ${item.species} </span>   
         </span>
            
        </div>
      </div>`);

  $('.character-list')[0].innerHTML = html.join('');
}).
catch(err => console.log(err));

// Assign id
const getCharacterById = character => {
    fetch(`https://rickandmortyapi.com/api/character/${character}`)
        .then(res => res.json())
        .then(data => {
            addCharacterToDOM(data);
        });
}

const addCharacterToDOM = character => {

    singleCharacter.innerHTML = `
    
        <div class = 'modal' id = 'modal'>

            <div class = 'modal-container'>
            
                <img src = '${character.image}' />
                  <div class = 'character-info' data-characterID = '${character.id}'>
        ${character.name}<br/>       
         ${character.status} - ${character.species}        
            
        </div>

                <div class = 'modal-container-info'>                
                  
                    <span>Gender: ${character.gender}</span>  <br/>
                    <span>Last Seen Location: ${character.location.name}</span><br/>
                    <span>Number of Episodes appeared: ${character.episode.length}</span>
                
                </div>

            </div>

        </div>

    `;
}

characters.addEventListener('click', e => {
var path = e.path || (e.composedPath && e.composedPath());
    const characterInfo = path.find(item => {
        if (item.classList) {
            return item.classList.contains('character-info');
        }
    });

    if (characterInfo) {
        const characterId = characterInfo.getAttribute('data-characterID');
        getCharacterById(characterId);
    }

});

window.addEventListener('click', e => {
    if (e.target === modal) {
        modal.style.display = 'none';
    }
});

// pagination navigation
let counter = 1;
next.addEventListener('click', () => {

    fetch(`https://rickandmortyapi.com/api/character/?page=${++counter}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            characters.innerHTML = data.results.map(item => `
            
                  <div class = 'character'>
             <div>
                      <img src="${item.image}" alt="" />
            </div>
            <div class = 'character-info' data-characterID = '${item.id}'>
                            ${item.name}<br/>       
                             ${item.status} - ${item.species}  
            </div>
        </div>
            
            `).join('');
        });
});

prev.addEventListener('click', () => {

    fetch(`https://rickandmortyapi.com/api/character/?page=${--counter}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            characters.innerHTML = data.results.map(item => `
            
                   <div class = 'character'>
             <div>
                      <img src="${item.image}" alt="" />
            </div>
            <div class = 'character-info' data-characterID = '${item.id}'>
                            ${item.name}<br/>       
                             ${item.status} - ${item.species}  
            </div>
        </div>
            
            `).join('');
        });
});


